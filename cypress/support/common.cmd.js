Cypress.Commands.add("openApp", () => {
  return cy.visit("/");
});

Cypress.Commands.add("isVisible", (selector) => {
  return cy.getDataTestID(selector).should("be.visible");
});

Cypress.Commands.add("isDisabled", (selector) => {
  return cy.getDataTestID(selector).should("be.disabled");
});

Cypress.Commands.add("isEnabled", (selector) => {
  return cy.getDataTestID(selector).should("not.be.disabled");
});

Cypress.Commands.add("isTextVisible", (selector) => {
  return cy.get(selector).should("be.visible");
});

Cypress.Commands.add("textClick", (selector) => {
  return cy.get(selector).click();
});

Cypress.Commands.add("isTextNotVisible", (selector) => {
  return cy.get(selector).should("not.exist");
});

Cypress.Commands.add("isButtonEnabled", (selector) => {
  return cy.get(selector).should("not.be.disabled");
});

Cypress.Commands.add("isButtonDisabled", (selector) => {
  return cy.get(selector).should("be.disabled");
});

Cypress.Commands.add("isXpathVisible", (selector) => {
  return cy.xpath(selector).should("be.visible");
});

Cypress.Commands.add("isXpathNotVisible", (selector) => {
  return cy.xpath(selector).should("not.exist");
});
