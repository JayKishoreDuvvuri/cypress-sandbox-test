// ### 1. Launch the Application
// ### 2. Type text into input checkbox
// ### 3. Click Add Todo submit button
// ### 4. Added text is visible
// ### 5. Show label is visible
// ### 6. All button is Disabled
// ### 7. Active and Completed buttons are enabled

import {
  addToDoButton,
  inputTextBox,
  textDecorationOne,
  textDecorationTwo,
  textDecorationThree,
  showLabel,
  allButton,
  activeButton,
  completedButton,
} from "../../selectors/locators";

import userdata from "../../fixtures/example.json";

describe("Add ToDo to verify added text", () => {
  before(function () {
    cy.openApp();
  });

  it("Type text in input check box and add ToDo", () => {
    cy.getDataTestID(inputTextBox).type(userdata.trainingSession);
    cy.getDataTestID(addToDoButton).click();
    cy.getDataTestID(inputTextBox).type(userdata.webinars);
    cy.getDataTestID(addToDoButton).click();
    cy.getDataTestID(inputTextBox).type(userdata.technicalDiscussion);
    cy.getDataTestID(addToDoButton).click();
  });

  it("Verify the text added is visible", () => {
    cy.isTextVisible(textDecorationOne);
    cy.isTextVisible(textDecorationTwo);
    cy.isTextVisible(textDecorationThree);
  });

  it("Verify show label is visible", () => {
    cy.isVisible(showLabel);
  });

  it("Check whether All button is disabled", () => {
    cy.isButtonDisabled(allButton);
  });

  it("Check whether Active and Completed buttons are enabled", () => {
    cy.isButtonEnabled(activeButton);
    cy.isButtonEnabled(completedButton);
  });
});
