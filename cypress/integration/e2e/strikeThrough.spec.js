// ### 1. Launch the Application
// ### 2. Type text into input checkbox
// ### 3. Click Add Todo submit button
// ### 4. Added text is visible
// ### 5. Check whether All button is disabled
// ### 6. Click on the added text to strike through
// ### 7. Check whether completed button is enabled and click it
// ### 8. Verify strike through text is visible after clicking completed button
// ### 9. Click on All button
// ###10. Verify strike through and non-strike through text is visible after clicking All button
// ###11. Click on Active button
// ###12. Verify only non-strike through text is visible after clicking All button
// ###13. Verify strike through text is not visible

import {
  activeButton,
  addToDoButton,
  allButton,
  completedButton,
  inputTextBox,
  textDecorationOne,
  textDecorationTwo,
  textDecorationThree,
  strikeThroughOne,
  strikeThroughTwo,
  strikeThroughThree,
} from "../../selectors/locators";

import userdata from "../../fixtures/example.json";

describe("Strike through text validation after clicking for ALL, Active and Completed Buttons", () => {
  before(function () {
    cy.openApp();
  });

  it("Type text in input check box and add ToDo", () => {
    cy.getDataTestID(inputTextBox).type(userdata.trainingSession);
    cy.getDataTestID(addToDoButton).click();
    cy.getDataTestID(inputTextBox).type(userdata.webinars);
    cy.getDataTestID(addToDoButton).click();
    cy.getDataTestID(inputTextBox).type(userdata.technicalDiscussion);
    cy.getDataTestID(addToDoButton).click();
  });

  it("Verify the text added is visible", () => {
    cy.isTextVisible(textDecorationOne);
    cy.isTextVisible(textDecorationTwo);
    cy.isTextVisible(textDecorationThree);
  });

  it("Check whether All button is disabled", () => {
    cy.isButtonDisabled(allButton);
  });

  it("Click on the added text to strike through", () => {
    cy.textClick(textDecorationOne);
    cy.textClick(textDecorationTwo);
  });

  it("Check whether completed button is enabled and click it", () => {
    cy.isButtonEnabled(completedButton);
    cy.get(completedButton).click();
  });

  it("Verify strike through text is visible after clicking completed button", () => {
    cy.isXpathVisible(strikeThroughOne);
    cy.isXpathVisible(strikeThroughTwo);
  });

  it("Click on All button", () => {
    cy.get(allButton).click();
  });

  it("Verify strike through and non-strike through text is visible after clicking All button", () => {
    cy.isXpathVisible(strikeThroughOne);
    cy.isXpathVisible(strikeThroughTwo);
    cy.isXpathVisible(strikeThroughThree);
  });

  it("Click on Active button", () => {
    cy.get(activeButton).click();
  });

  it("Verify only non-strike through text is visible after clicking Active button and check strike through is not visible", () => {
    cy.isXpathVisible(strikeThroughThree);
    cy.isXpathNotVisible(strikeThroughOne);
    cy.isXpathNotVisible(strikeThroughTwo);
  });
});
