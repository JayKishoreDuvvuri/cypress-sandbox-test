// ### 1. Launch the Application
// ### 2. Type text into input checkbox
// ### 3. Click Add Todo submit button
// ### 4. Added text is visible
// ### 5. Check all button is disabled
// ### 6. Check completed button is enabled and click it
// ### 7. Check whether completed button is disabled now
// ### 8. Verify the added text earlier is not visible now
// ### 9. Check show label is visible
// ###10. Check All and Active buttons are enabled
// ###11. Input check box and Add ToDo button are enabled
// ###12. Click on All button
// ###13. Click on the added text to strike through
// ###14. Verify Completed button is enabled and click it
// ###15. Input check box and Add ToDo button are enabled
// ###16. Verify strike through text is visible after clicking completed button

import {
  activeButton,
  addToDoButton,
  allButton,
  completedButton,
  inputTextBox,
  showLabel,
  textDecorationOne,
  textDecorationTwo,
  textDecorationThree,
  strikeThroughOne,
  strikeThroughTwo,
} from "../../selectors/locators";

import userdata from "../../fixtures/example.json";

describe("Completed Button", () => {
  before(function () {
    cy.openApp();
  });

  it("Type text in input check box and add ToDo", () => {
    cy.getDataTestID(inputTextBox).type(userdata.trainingSession);
    cy.getDataTestID(addToDoButton).click();
    cy.getDataTestID(inputTextBox).type(userdata.webinars);
    cy.getDataTestID(addToDoButton).click();
    cy.getDataTestID(inputTextBox).type(userdata.technicalDiscussion);
    cy.getDataTestID(addToDoButton).click();
  });

  it("Verify the text added is visible", () => {
    cy.isTextVisible(textDecorationOne);
    cy.isTextVisible(textDecorationTwo);
    cy.isTextVisible(textDecorationThree);
  });

  it("Check whether All button is disabled", () => {
    cy.isButtonDisabled(allButton);
  });

  it("Check whether completed button is enabled and click it", () => {
    cy.isButtonEnabled(completedButton);
    cy.get(completedButton).click();
  });

  it("Check whether completed button is disabled now", () => {
    cy.isButtonDisabled(completedButton);
  });

  it("Verify the text added earlier is not visible", () => {
    cy.isTextNotVisible(textDecorationOne);
    cy.isTextNotVisible(textDecorationTwo);
    cy.isTextNotVisible(textDecorationThree);
  });

  it("Verify show label is visible", () => {
    cy.isVisible(showLabel);
  });

  it("Check whether all and active buttons are enabled now", () => {
    cy.isButtonEnabled(allButton);
    cy.isButtonEnabled(activeButton);
  });

  it("Verify input text box is enabled", () => {
    cy.isEnabled(inputTextBox);
  });

  it("Verify Add Todo button is enabled", () => {
    cy.isEnabled(addToDoButton);
  });

  it("Click on All button", () => {
    cy.get(allButton).click();
  });

  it("Click on the added text to strike through", () => {
    cy.textClick(textDecorationOne);
    cy.textClick(textDecorationTwo);
  });

  it("Verify required text is visible with strike through", () => {
    cy.isXpathVisible(strikeThroughOne);
    cy.isXpathVisible(strikeThroughTwo);
  });

  it("Verify Completed button is enabled and click it", () => {
    cy.isButtonEnabled(completedButton);
    cy.get(completedButton).click();
  });

  it("Verify strike through text is visible after clicking completed button", () => {
    cy.isXpathVisible(strikeThroughOne);
    cy.isXpathVisible(strikeThroughTwo);
  });
});
