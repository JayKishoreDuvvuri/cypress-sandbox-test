// ### 1. Launch the Application
// ### 2. Type text into input checkbox
// ### 3. Click Add Todo submit button
// ### 4. Check all button is disabled
// ### 5. Check active button is enabled and click it
// ### 6. Check active button is disabled
// ### 7. Check all and Completed buttons are enabled
// ### 8. Click on all button and verify all button is disabled
// ### 9. Verify the added text earlier is visible
// ### 9. Verify active and completed buttons are enabled
// ###10. Verify Show label is visible
// ###11. Check whether Input check box is enabled
// ###12. Verify Add Todo button is enabled

import {
  activeButton,
  addToDoButton,
  allButton,
  completedButton,
  inputTextBox,
  showLabel,
  textDecorationOne,
  textDecorationTwo,
  textDecorationThree,
} from "../../selectors/locators";

import userdata from "../../fixtures/example.json";

describe("All Button", () => {
  before(function () {
    cy.openApp();
  });

  it("Type text in input check box and add ToDo", () => {
    cy.getDataTestID(inputTextBox).type(userdata.trainingSession);
    cy.getDataTestID(addToDoButton).click();
    cy.getDataTestID(inputTextBox).type(userdata.webinars);
    cy.getDataTestID(addToDoButton).click();
    cy.getDataTestID(inputTextBox).type(userdata.technicalDiscussion);
    cy.getDataTestID(addToDoButton).click();
  });

  it("Check whether All button is disabled", () => {
    cy.isButtonDisabled(allButton);
  });

  it("Check whether active button is enabled and click it", () => {
    cy.isButtonEnabled(activeButton);
    cy.get(activeButton).click();
  });

  it("Check whether active button is disabled now", () => {
    cy.isButtonDisabled(activeButton);
  });

  it("Check whether all and completed buttons are enabled now", () => {
    cy.isButtonEnabled(allButton);
    cy.isButtonEnabled(completedButton);
  });

  it("Click on all button and verify all button is disabled", () => {
    cy.get(allButton).click();
    cy.isButtonDisabled(allButton);
  });

  it("Verify the text added is visible", () => {
    cy.isTextVisible(textDecorationOne);
    cy.isTextVisible(textDecorationTwo);
    cy.isTextVisible(textDecorationThree);
  });

  it("Check whether active and completed buttons are enabled now", () => {
    cy.isButtonEnabled(activeButton);
    cy.isButtonEnabled(completedButton);
  });

  it("Verify show label is visible", () => {
    cy.isVisible(showLabel);
  });

  it("Verify input text box is enabled", () => {
    cy.isEnabled(inputTextBox);
  });

  it("Verify Add Todo button is enabled", () => {
    cy.isEnabled(addToDoButton);
  });
});
