// ### 1. Launch the Application
// ### 2. Check Input text box visible
// ### 3. Check Add Todo Button enabled
// ### 4. Check Show label visible
// ### 5. Check all button is disabled
// ### 6. Check Active and Copmpleted buttons are enabled

import {
  activeButton,
  addToDoButton,
  allButton,
  completedButton,
  inputTextBox,
  showLabel,
} from "../../selectors/locators";

describe("User arrives to Landing Page", () => {
  before(function () {
    cy.openApp();
  });

  it("Check input text box, submit button, Show label, All, Active and Completed buttons are visible and enabled", () => {
    cy.isVisible(inputTextBox);
    cy.isEnabled(inputTextBox);
    cy.isEnabled(addToDoButton);
    cy.isVisible(showLabel);
    cy.isButtonDisabled(allButton);
    cy.isButtonEnabled(activeButton);
    cy.isButtonEnabled(completedButton);
  });
});
