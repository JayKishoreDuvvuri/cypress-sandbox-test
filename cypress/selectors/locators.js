module.exports = {
  inputTextBox: "input-text-box",
  addToDoButton: "submit-button",
  showLabel: "show-label",
  allButton: "div:nth-child(3) > button:nth-child(2)",
  activeButton: "div:nth-child(3) > button:nth-child(3)",
  completedButton: "div:nth-child(3) > button:nth-child(4)",
  textDecorationOne: "li:nth-child(1)",
  textDecorationTwo: "li:nth-child(2)",
  textDecorationThree: "li:nth-child(3)",
  strikeThroughOne:
    '(//li[@data-test-id="Completed-Task--Text" and @style="text-decoration: line-through;"])[1]',
  strikeThroughTwo:
    '(//li[@data-test-id="Completed-Task--Text" and @style="text-decoration: line-through;"])[2]',
  strikeThroughThree:
    '//li[@data-test-id="Completed-Task--Text" and @style="text-decoration: none;"]',
};
